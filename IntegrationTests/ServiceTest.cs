﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DescriptionServiceClient;
using DescriptionServiceClient.Model;
using NFluent;
using NUnit.Framework;

namespace IntegrationTests
{
    public class ServiceTest
    {
        [Test]
        public void Test()
        {
            var descriptionService = new DescriptionService();
            var hotels = descriptionService.GetHotels(1903).Hotels.Take(20).ToList();

            Check.That(hotels).HasSize(20);
            var addressInfos = hotels.Select(x => x.Address).ToArray();
            var contactInfos = hotels.Select(x => x.Contact).ToArray();
            var registrationInfos = hotels.Select(x => x.Registration).ToArray();
            var ageSettings = hotels.Select(x => x.AgeSettings).ToArray();
            var amenities = hotels.Where(x => x.Amenities != null).SelectMany(x => x.Amenities.SelectMany(s=>s.Amenities)).ToArray();
            

            Check.That(hotels.Extracting("PropertyName")).Contains("Lisboa Plaza Hotel");
            Check.That(hotels.Extracting("PropertyType")).Contains("Hotel");
            Check.That(hotels.Extracting("PropertyWebsite")).Contains("http://www.eh-reservations.com/");
            Check.That(hotels.Extracting("Phone")).Contains("+351 213 218218");
            Check.That(hotels.Extracting("StarRating")).Contains(4);
            Check.That(hotels.Extracting("RoomsCount")).Contains(112);
            Check.That(hotels.Extracting("TimeZone")).Contains("Europe/Paris");
            Check.That(hotels.First().Currencies).Contains("EUR");
            Check.That(hotels.First().CreditCards).Contains("MasterCard");

            Check.That(addressInfos.Extracting("Country")).Contains("FR");
            Check.That(addressInfos.Extracting("Street")).Contains("40, avenue de Friedland");
            Check.That(addressInfos.Extracting("City")).Contains("Paris");
            Check.That(addressInfos.Extracting("Region")).Contains("Ile de France");
            Check.That(addressInfos.Extracting("Zip")).Contains("75006");
            Check.That(addressInfos.Extracting("Gps")).Contains("38.7055168151856, -9.16031646728516");

            Check.That(contactInfos.Extracting("Name")).Contains("Anna");
            Check.That(contactInfos.Extracting("Phone")).Contains("+351 213 218218");
            Check.That(contactInfos.Extracting("Fax")).Contains("+351 213 471630");
            Check.That(contactInfos.Extracting("Email")).Contains("accueil@hoteleurope.net");

            Check.That(registrationInfos.Extracting("CheckInTime")).Contains("13:00");
            Check.That(registrationInfos.Extracting("CheckOutTime")).Contains("11:00");

            Check.That(ageSettings.Extracting("Adult")).Contains("12");
            Check.That(ageSettings.Extracting("Children")).Contains("2-12");
            Check.That(ageSettings.Extracting("Infant")).Contains("0-2");

            Check.That(amenities).Contains("LocalPhone", "Bar", "FreeInternetAccess");

            var rooms = hotels.Where(x => x.Rooms != null && x.Rooms.Any()).SelectMany(x => x.Rooms).ToArray();
            Check.That(rooms).Not.IsNullOrEmpty();
            Check.That(rooms.Extracting("Name")).Contains("Single room - Superior");
            Check.That(rooms.Extracting("Count")).Contains(10);
            Check.That(rooms.Extracting("Type")).Contains("Double room");
            Check.That(rooms.Extracting("Category")).Contains("Romantic");
            Check.That(rooms.Extracting("Qualifier")).Contains("with bath");

            var beds = rooms.Where(x => x.Beds != null && x.Beds.Any()).SelectMany(x => x.Beds).ToArray();
            Check.That(beds).Not.IsNullOrEmpty();
            Check.That(beds.Extracting("Type")).Contains("Double");
            Check.That(beds.Extracting("Count")).Contains(1);

            var occupancies = rooms.Where(x => x.Occupancies != null && x.Occupancies.Any()).SelectMany(x => x.Occupancies).ToArray();
            Check.That(occupancies).Not.IsNullOrEmpty();
            Check.That(occupancies.Extracting("MaxAdults")).Contains(2);
            Check.That(occupancies.Extracting("MaxChildren")).Contains(2);
            Check.That(occupancies.Extracting("MaxAdults")).Contains(2);
            Check.That(occupancies.Extracting("MaxInfants")).Contains(1);

            var rates = hotels.Where(x => x.Rates != null).SelectMany(x => x.Rates).ToArray();
            Check.That(rates).Not.IsNullOrEmpty();
            Check.That(rates.Extracting("Name")).Contains("LAST MINUTE RATE");
            Check.That(rates.Extracting("Code")).Contains(57553);
            Check.That(rates.Extracting("Currency")).Contains("EUR");

            var roomAmenities =
                rooms.Where(x => x.Amenities != null && x.Amenities.Any()).SelectMany(x => x.Amenities.SelectMany(s=>s.Amenities)).ToArray();
            Check.That(roomAmenities).Not.IsNullOrEmpty();
            Check.That(roomAmenities).Contains("MiniBar", "NonSmoking");
        }

        [TestCase(1959)]
        [TestCase(2378)]
        [TestCase(2403)]
        [TestCase(2805)]
        public void TestOneHotel(int hotelId)
        {
            var descriptionService = new DescriptionService();
            var hotels = descriptionService.GetHotels(1903, new[] { (uint)hotelId });

            var xs = new XmlSerializer(typeof(List<HotelInfo>),
                new[]
                {
                    typeof(Failure),
                });

            var cnt = 0;
            foreach (var lot in hotels.Hotels.ByLot(10))
            {
                cnt++;

                using (var wr = new StreamWriter($"C:\\Temp\\hotel_{hotelId}.xml"))
                {
                    xs.Serialize(wr, lot.ToList());
                }
            }
        }
    }
}
