﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using DescriptionServiceClient.Model;
using static System.String;

namespace DescriptionServiceClient.Extractions
{
    public abstract class ExtractHotel
    {
        private static uint _groupId = 1903;

        public void Run(uint[] hotelIds)
        {
            var descriptionService = new DescriptionService();
            var hotels = descriptionService.GetHotels(_groupId, hotelIds);

            Persist(hotelIds, hotels);
        }

        protected abstract void Persist(uint[] hotelIds, Result result);
    }
}