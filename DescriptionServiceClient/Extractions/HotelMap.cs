using CsvHelper.Configuration;

namespace DescriptionServiceClient.Extractions
{
    public sealed class HotelMap : CsvClassMap<HotelInfoCsv>
    {
        public HotelMap()
        {
            Map(m => m.HotelId).Name("Hotel.Set");
            Map(m => m.PropertyName).Name("Hotel.Name");
            Map(m => m.StarRating).Name("Hotel.Rating");
            Map(m => m.RoomsCount).Name("Hotel.RoomCount");
            Map(m => m.PropertyType).Name("Hotel.AccomodationType");
            Map(m => m.Street).Name("Hotel.AddressLines");
            Map(m => m.Zip).Name("Hotel.PostalCode");
            Map(m => m.City).Name("Hotel.CityName");
            Map(m => m.Country).Name("Hotel.CountryCode");
            Map(m => m.Phone).Name("Hotel.Phone");
            Map(m => m.Fax).Name("Hotel.Fax");
            Map(m => m.PropertyWebsite).Name("Hotel.WebSite");
            Map(m => m.Email).Name("Hotel.Email");
            Map(m => m.Name).Name("Hotel.ManagerName");
            Map(m => m.Currencies).Name("Hotel.Currency");
            Map(m => m.Gps).Name("Hotel.GpsPosition");
        }
    }
}