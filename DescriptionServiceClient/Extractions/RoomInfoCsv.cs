namespace DescriptionServiceClient.Extractions
{
    public class RoomInfoCsv
    {
        public object HotelId { get; set; }
        public object Count { get; set; }
        public object Type { get; set; }
        public object Category { get; set; }
        public object Qualifier { get; set; }
        public object Occupancies { get; set; }
    }
}