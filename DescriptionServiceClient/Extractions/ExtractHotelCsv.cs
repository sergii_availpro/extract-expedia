﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel.Description;
using System.Text;
using System.Xml.Serialization;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using DescriptionServiceClient.Model;
using static System.String;

namespace DescriptionServiceClient.Extractions
{
    public class ExtractHotelCsv : ExtractHotel
    {
        protected override void Persist(uint[] hotelIds, Result hotels)
        {
            var resultPathHotels = $"hotels_{Join("_", hotelIds)}.csv";
            var resultPathRooms = $"rooms_{Join("_", hotelIds)}.csv";
            var resultPathRates = $"rates_{Join("_", hotelIds)}.csv";
            var resultPathJoined = $"joined_{Join("_", hotelIds)}.csv";

            using (var file = File.CreateText(resultPathHotels))
            {
                var writer = new CsvWriter(file, new CsvConfiguration());
                writer.Configuration.RegisterClassMap<HotelMap>();

                writer.WriteRecords(hotels.Hotels.Select(
                    x => new HotelInfoCsv
                    {
                        HotelId = $"H{x.HotelId}",
                        Phone = x.Contact.Phone,
                        PropertyName = x.PropertyName,
                        StarRating = x.StarRating,
                        RoomsCount = x.RoomsCount,
                        PropertyType = x.PropertyType,
                        Street = x.Address.Street,
                        Zip = x.Address.Zip,
                        City = x.Address.City,
                        Country = x.Address.Country,
                        Fax = x.Contact.Fax,
                        PropertyWebsite = x.PropertyWebsite,
                        Email = x.Contact.Email,
                        Name = x.Contact.Name,
                        Currencies = Join(",", x.Currencies),
                        Gps = x.Address.Gps
                    }
                    ));
            }

            using (var file = File.CreateText(resultPathRooms))
            {
                var writer = new CsvWriter(file);
                writer.Configuration.RegisterClassMap<RoomsMap>();

                writer.WriteRecords(
                    hotels.Hotels.SelectMany(x => x.Rooms.Select(s => new RoomInfoCsv
                    {
                        HotelId = $"H{x.HotelId}",
                        Category = s.Category,
                        Count = s.Count,
                        Occupancies = Join(",", s.Occupancies.Select(FormatOccupancy)),
                        Qualifier = s.Qualifier,
                        Type = s.Type
                    })));
            }

            using (var file = File.CreateText(resultPathRates))
            {
                var writer = new CsvWriter(file);
                writer.Configuration.RegisterClassMap<RatesMap>();

                writer.WriteRecords(
                    hotels.Hotels.SelectMany(x => x.Rates.Select(s => new RateInfoCsv
                    {
                        HotelId = $"H{x.HotelId}",
                        Type = "PublicPrice",
                        Qualifier = "RoomOnly",
                        Name = s.Name,
                        Code = s.Code,
                        Currency = s.Currency
                    })));
            }

            using (var file = File.CreateText(resultPathJoined))
            {
                var cartesian = from hotel in hotels.Hotels
                                from room in hotel.Rooms
                                from rate in hotel.Rates
                                select new { hotel, room, rate };

                var items = cartesian.Select(x =>
                    new HotelInfoJoinedCsv
                    {
                        Hotel_ID = x.hotel.HotelPartnerId,
                        Hotel_Code = $"H{x.hotel.HotelId}",
                        Hotel_Name = x.hotel.PropertyName,
                        Room_Type_Code = x.room.Type,
                        Smoking = "NonSmoking",
                        Bed_Type = ConvertBedType(x.room.Beds),
                        Max_Occupancy = x.room.Occupancies.Max(s=>s.MaxAdults+s.MaxChildren+s.MaxInfants),
                        Max_Adults = x.room.Occupancies.Max(s=>s.MaxAdults),
                        Max_Children = x.room.Occupancies.Max(s=>s.MaxChildren),
                        Max_Infants = x.room.Occupancies.Max(s=>s.MaxInfants),
                        Room_Type_Name = x.room.Name,

                        RNS_Flag = "Yes",
                        RNS_Room_Type = x.room.Type,
                        RNS_Quality = x.room.Category,
                        RNS_Bed_Type = ConvertBedTypeRns(x.room.Beds),
                        RNS_Smoking_Pref = "NonSmoking",
                        RNS_View = "No View",

                        Hotel_Collect_Rate_Plan_Code = x.rate.Code,
                        Rate_Plan_Name = x.rate.Name,
                        Rate_Plan_Type = x.rate.Qualifier,
                        Business_Model = "Agency",
                        ARI_Rate_Plan = "No",
                        Rate_Plan_Status = "Active",
                        
                        Amenities = x.room.Amenities.SelectMany(s=>s.Amenities).ToArray(),

                        VAT = x.hotel.Vat
                    }).ToArray();

                var csv = new CsvWriter(file);
                var props = typeof(HotelInfoJoinedCsv).GetProperties();
                var maxAmenities = items.Max(x => x.Amenities.Length);

                foreach (var propertyInfo in props)
                {
                    if (propertyInfo.Name == "Amenities")
                    {
                        for (int i = 0; i < maxAmenities; i++)
                        {
                            csv.WriteField($"Value Add {i + 1}");
                        }
                    }
                    else
                    {
                        csv.WriteField(propertyInfo.Name.Replace("_", " "));
                    }
                }
                csv.NextRecord();
                foreach (var item in items)
                {
                    foreach (var propertyInfo in props)
                    {
                        if (propertyInfo.Name == "Amenities")
                        {
                            for (var i = 0; i < maxAmenities; i++)
                            {
                                csv.WriteField(i < item.Amenities.Length ? item.Amenities[i] : "");
                            }
                        }
                        else
                        {
                            csv.WriteField(propertyInfo.GetValue(item) ?? "");
                        }
                    }
                    csv.NextRecord();
                }
            }
        }

        private object ConvertBedTypeRns(Bed[] beds)
        {
            if (beds == null || beds.Length == 0)
            {
                return "1 Double Bed";
            }
            if (beds.Length > 1)
            {
                return "Multiple Beds";
            }
            var bed = beds.First();
            if (bed.Type == "Double" || bed.Type == "Single" || bed.Type == "Queen" || bed.Type == "Multiple")
            {
                if (bed.Count == 1)
                {
                    return $"{bed.Count} {bed.Type} Bed";
                }
                return $"{bed.Count} {bed.Type} Beds";
            }

            return $"{bed.Count} {bed.Type}";
        }

        private string ConvertBedType(Bed[] beds)
        {
            if (beds == null || beds.Length == 0)
            {
                return "1 double bed";
            }

            return Join(",", beds.Select(x => $"{x.Count} {x.Type}"));
        }

        private static string FormatOccupancy(Occupancy x)
        {
            var res = new List<string>();
            if (x.MaxAdults != 0)
            {
                res.Add($"{x.MaxAdults}A");
            }
            if (x.MaxChildren != 0)
            {
                res.Add($"{x.MaxChildren}C");
            }
            if (x.MaxInfants != 0)
            {
                res.Add($"{x.MaxInfants}I");
            }
            return Join("+", res);
        }
    }
}