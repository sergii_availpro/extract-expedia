﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using DescriptionServiceClient.Model;
using static System.String;

namespace DescriptionServiceClient.Extractions
{
    public class ExtractHotelXml : ExtractHotel
    {
        protected override void Persist(uint[] hotelIds, Result result)
        {
            var xs = new XmlSerializer(typeof(List<HotelInfo>),
                new[]
                {
                    typeof (Failure),
                });

            var resultPath = $"hotel_{Join("_", hotelIds)}.xml";

            using (var wr = new StreamWriter(resultPath))
            {
                xs.Serialize(wr, result.Hotels.ToList());
            }

            Console.WriteLine($"Done. The result file : {resultPath}");
        }

    }
}