using CsvHelper.Configuration;

namespace DescriptionServiceClient.Extractions
{
    public sealed class RatesMap : CsvClassMap<RateInfoCsv>
    {
        public RatesMap()
        {
            Map(m => m.HotelId).Name("Hotel.Set");
            Map(m => m.Type).Name("Rate.Type");
            Map(m => m.Qualifier).Name("Rate.Qualifier");
            Map(m => m.Name).Name("Rate.Name");
            Map(m => m.Code).Name("Rate.Code");
            Map(m => m.Currency).Name("Rate.Currency");
        }
    }
}