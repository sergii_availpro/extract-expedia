namespace DescriptionServiceClient.Extractions
{
    public class RateInfoCsv
    {
        public object HotelId { get; set; }
        public object Type { get; set; }
        public object Qualifier { get; set; }
        public object Name { get; set; }
        public object Code { get; set; }
        public object Currency { get; set; }
    }
}