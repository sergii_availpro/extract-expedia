using CsvHelper.Configuration;

namespace DescriptionServiceClient.Extractions
{
    public sealed class RoomsMap : CsvClassMap<RoomInfoCsv>
    {
        public RoomsMap()
        {
            Map(m => m.HotelId).Name("Hotel.Set");
            Map(m => m.Count).Name("Room.Count");
            Map(m => m.Type).Name("Room.Type");
            Map(m => m.Category).Name("Room.Category");
            Map(m => m.Qualifier).Name("Room.Qualifier");
            Map(m => m.Occupancies).Name("Room.Occupancies");
        }
    }
}