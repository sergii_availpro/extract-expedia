namespace DescriptionServiceClient.Extractions
{
    public class HotelInfoCsv
    {
        public object HotelId { get; set; }
        public object Phone { get; set; }
        public object PropertyName { get; set; }
        public object StarRating { get; set; }
        public object RoomsCount { get; set; }
        public object PropertyType { get; set; }
        public object Street { get; set; }
        public object Zip { get; set; }
        public object City { get; set; }
        public object Country { get; set; }
        public object Fax { get; set; }
        public object PropertyWebsite { get; set; }
        public object Email { get; set; }
        public object Name { get; set; }
        public object Currencies { get; set; }
        public object Gps { get; set; }
    }
}