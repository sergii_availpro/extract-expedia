using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using DescriptionServiceClient.Model;

namespace DescriptionServiceClient.Extractions
{
    public class ExtractAllHotelsForAGroup
    {
        //Expedia
        private const uint GroupId = 1903;
        private readonly string _defaultPath = $@"ExpediaExtract_{DateTime.Now:yyyyMMdd}.xml";
        private readonly XmlSerializer _serializer;
        private readonly DescriptionService _descriptionService;

        public ExtractAllHotelsForAGroup()
        {
            _serializer = new XmlSerializer(typeof(List<HotelInfo>),
                new[]
                {
                    typeof (Failure),
                });

            _descriptionService = new DescriptionService();
        }

        public void Run(string resultFilePath = null)
        {
            resultFilePath = resultFilePath ?? _defaultPath;
            
            var hotels = File.Exists(resultFilePath)
                ? CompleteLatestExtraction(resultFilePath)
                : ExtractAll();

            Console.WriteLine($"Total hotels : {hotels.Count}. Failed to load hotels : {hotels.Count(x => x is Failure)}");

            using (var wr = new StreamWriter(resultFilePath))
            {
                _serializer.Serialize(wr, hotels);
            }
        }

        private List<HotelInfo> ExtractAll()
        {
            var result = _descriptionService.GetHotels(GroupId);

            var hotels = new List<HotelInfo>();
            foreach (var hotelInfo in result.Hotels.Take(50))
            {
                hotels.Add(hotelInfo);
                Console.WriteLine($"{hotels.Count} of {result.HotelsCount} loaded");
            }
            return hotels;
        }

        private List<HotelInfo> CompleteLatestExtraction(string resultPath)
        {
            Console.WriteLine("Completing latest extraction");
            List<HotelInfo> hotels;
            using (var reader = new StreamReader(resultPath))
            {
                hotels = (List<HotelInfo>)_serializer.Deserialize(reader);
            }

            var hotelsInFailure = hotels.Where(x => x is Failure).Cast<Failure>().ToList();

            var idToPosition = hotels.Select((x, i) => new { Index = i, HotelInfo = x })
                .Where(x => x.HotelInfo is Failure)
                .ToList()
                .ToDictionary(x => ((Failure)x.HotelInfo).HotelId, x => x.Index);

            Console.WriteLine($"Total hotels : {hotels.Count}. Failed to load hotels : {hotelsInFailure.Count}");
            Console.WriteLine($"Retrying to load {hotelsInFailure.Count} hotels");

            var retryingHotels = _descriptionService.GetHotels(GroupId, hotelsInFailure.Select(x => x.HotelId).ToArray());
            var retryingHotelsLoaded = new List<HotelInfo>();
            foreach (var hotelInfo in retryingHotels.Hotels)
            {
                retryingHotelsLoaded.Add(hotelInfo);
                hotels[idToPosition[hotelInfo.HotelId]] = hotelInfo;

                Console.WriteLine($"{retryingHotelsLoaded.Count} of {retryingHotels.HotelsCount} loaded");
            }
            return hotels;
        }

    }
}