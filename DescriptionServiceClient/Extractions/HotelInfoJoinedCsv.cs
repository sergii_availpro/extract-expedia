using DescriptionServiceClient.Model;

namespace DescriptionServiceClient.Extractions
{
    public class HotelInfoJoinedCsv
    {
        public object Hotel_ID { get; set; }
        public object Hotel_Code { get; set; }
        public object Hotel_Name { get; set; }
        public object Room_Type_Code { get; set; }
        public object Smoking { get; set; }
        public object Bed_Type { get; set; }
        public object Max_Occupancy { get; set; }
        public object Max_Adults { get; set; }
        public object Max_Children { get; set; }
        public object Max_Infants { get; set; }
        public object Min_Age_Adults { get; set; }
        public object Min_Age_Children { get; set; }
        public object Min_Age_Infants { get; set; }
        public object Room_Type_Name { get; set; }
        public object RNS_Flag { get; set; }
        public object RNS_Room_Type { get; set; }
        public object RNS_Quality { get; set; }
        public object RNS_Brand_Attribute { get; set; }
        public object RNS_Bed_Type { get; set; }
        public object RNS_Bedroom { get; set; }
        public object RNS_Accessibility { get; set; }
        public object RNS_Smoking_Pref { get; set; }
        public object RNS_View { get; set; }
        public object RNS_Feature_Amenity { get; set; }
        public object RNS_Area { get; set; }
        public object Hotel_Collect_Rate_Plan_Code { get; set; }
        public object Rate_Plan_Name { get; set; }
        public object Rate_Plan_Type { get; set; }
        public object Business_Model { get; set; }
        public object ARI_Rate_Plan { get; set; }
        public object Rate_Plan_Status { get; set; }
        public string[] Amenities { get; set; }
        public object Cancellation_Window_Hours { get; set; }
        public object Outside_Window_Penalty { get; set; }
        public object Inside_Window_Penalty { get; set; }
        public object Min_Length_of_Stay { get; set; }
        public object Max_Length_of_Stay { get; set; }
        public object Advance_Purchase_Minimum { get; set; }
        public object Advance_Purchase_Maximum { get; set; }
        public object Mobile_Users_Only { get; set; }
        public object Waive_Taxes_Enabled { get; set; }
        public object VAT { get; set; }
        
    }
}