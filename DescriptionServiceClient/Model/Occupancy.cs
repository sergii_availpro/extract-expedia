namespace DescriptionServiceClient.Model
{
    public class Occupancy
    {
        public int MaxAdults { get; set; }
        public int MaxChildren { get; set; }
        public int MaxInfants { get; set; }
    }
}