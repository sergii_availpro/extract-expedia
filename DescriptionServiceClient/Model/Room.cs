using System.Xml.Serialization;
using DescriptionServiceClient.Siriona.Availpro.Description;

namespace DescriptionServiceClient.Model
{
    public class Room
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public Bed[] Beds { get; set; }
        public Occupancy[] Occupancies { get; set; }
        public AmenityGroup[] Amenities { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string Qualifier { get; set; }
    }
}