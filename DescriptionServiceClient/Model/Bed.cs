namespace DescriptionServiceClient.Model
{
    public class Bed
    {
        public string Type { get; set; }
        public int Count { get; set; }
    }
}