namespace DescriptionServiceClient.Model
{
    public class AmenityGroup
    {
        public string Name { get; set; }
        public string [] Amenities { get; set; }
    }
}