namespace DescriptionServiceClient.Model
{
    public class Contact
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}