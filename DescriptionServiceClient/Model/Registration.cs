using System;

namespace DescriptionServiceClient.Model
{
    public class Registration
    {
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
    }
}