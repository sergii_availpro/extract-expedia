namespace DescriptionServiceClient.Model
{
    public class AgeSettings
    {
        public string Adult { get; set; }
        public string Children { get; set; }
        public string Infant { get; set; }
    }
}