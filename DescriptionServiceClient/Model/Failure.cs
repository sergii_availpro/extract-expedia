namespace DescriptionServiceClient.Model
{
    public class Failure : HotelInfo
    {
        public override uint HotelId { get; set; }
        public string Error { get; set; }
    }
}