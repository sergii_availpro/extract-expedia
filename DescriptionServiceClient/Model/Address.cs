namespace DescriptionServiceClient.Model
{
    public class Address
    {
        public string Country { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Zip { get; set; }
        public string Gps { get; set; }
    }
}