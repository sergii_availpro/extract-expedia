﻿using System.Xml.Serialization;

namespace DescriptionServiceClient.Model
{
    public class HotelInfo
    {
        [XmlIgnore]
        public virtual uint HotelId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyType { get; set; }
        public string Phone { get; set; }
        public int RoomsCount { get; set; }
        public string PropertyWebsite { get; set; }
        public int StarRating { get; set; }
        public string[] Currencies { get; set; }
        public string[] CreditCards { get; set; }
        public string TimeZone { get; set; }
        public Address Address { get; set; }
        public Contact Contact { get; set; }
        public Registration Registration { get; set; }
        public AgeSettings AgeSettings { get; set; }
        public AmenityGroup[] Amenities { get; set; }
        public Room[] Rooms { get; set; }
        public Rate[] Rates { get; set; }
        public string HotelPartnerId { get; set; }
        public string Vat { get; set; }
    }

    public class Rate
    {
        public string Name { get; set; }
        public int Code { get; set; }
        public string Currency { get; set; }
        public string Qualifier { get; set; }
    }
}