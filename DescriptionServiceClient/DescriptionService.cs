﻿using System;
using System.Collections.Generic;
using System.Linq;
using DescriptionServiceClient.Model;
using DescriptionServiceClient.Siriona.Availpro.Description;
using Siriona.Availpro.Security.Consumer.OAuth;
using Contact = DescriptionServiceClient.Model.Contact;
using Occupancy = DescriptionServiceClient.Model.Occupancy;

namespace DescriptionServiceClient
{
    public class DescriptionService
    {
        private readonly DescriptionServiceSoapClient _client;
        private readonly ChannelRequestProvider _channelRequestProvider;
        private const string LanguageCode = "EN";
        private const string CountryCode = "FR";

        public DescriptionService()
        {
            OAuthUtility.Configure();
            _client = new DescriptionServiceSoapClient();
            _channelRequestProvider = new ChannelRequestProvider();
        }

        public Result GetHotels(uint groupId, uint[] hotelIds = null)
        {
            var impersonation = new Impersonation();

            if (hotelIds == null)
            {
                hotelIds = GetAllHotelsForGroup(groupId, impersonation);
            }

            return new Result
            {
                HotelsCount = hotelIds.Length,
                Hotels = GetHotelInfos(groupId, impersonation, hotelIds)
            };
        }

        private uint[] GetAllHotelsForGroup(uint groupId, Impersonation impersonation)
        {
            var groupDescription = Validate(_client.GetGroup(groupId, new[] { GroupProperty.Hotels }, LanguageCode, CountryCode, impersonation));
            return groupDescription.groups.Single().hotels.Select(x => x.id).ToArray();
        }

        private IEnumerable<HotelInfo> GetHotelInfos(uint groupId, Impersonation impersonation, uint[] hotelIds)
        {
            foreach (var tenHotels in hotelIds.ByLot(10))
            {
                var tenHotelIds = tenHotels.ToArray();
                var hotelsDescription = Validate(_client.GetHotels(groupId, tenHotelIds,
                    new[]
                    {
                        HotelProperty.Information,
                        HotelProperty.Booking,
                        HotelProperty.Rooms,
                        HotelProperty.Services,
                        HotelProperty.Contact,
                        HotelProperty.Location,
                        HotelProperty.Reception,
                        HotelProperty.Rates
                    },
                    LanguageCode, CountryCode, impersonation));

                var channelRequest = _channelRequestProvider.GetChannelRequest(groupId, tenHotelIds).ToList();
                //var roomIds = hotelsDescription.hotels.Where(x => x.rooms != null).SelectMany(x => x.rooms.Select(s => s.id)).ToArray();
                var roomIds = channelRequest.SelectMany(x => x.RoomIds).ToArray();
                var roomsDescription = Validate(_client.GetRooms(groupId, roomIds,
                    new[]
                    {
                            RoomProperty.Information,
                            RoomProperty.Occupancies,
                            RoomProperty.Services
                    }, LanguageCode, CountryCode,
                    impersonation));
                var roomsDescriptionDictionary = roomsDescription.rooms.ToDictionary(x => x.id, x => x);

                //var rateIds =
                //    hotelsDescription.hotels.Where(x => x.rates != null)
                //        .SelectMany(x => x.rates.Select(s => s.id))
                //        .ToArray();
                var rateIds = channelRequest.SelectMany(x => x.RateIds).ToArray();
                var ratesDescription =
                    Validate(_client.GetRates(groupId, rateIds, new[] {RateProperty.Information}, LanguageCode, CountryCode,
                        impersonation));
                var ratesDescriptionDictionary = ratesDescription.rates.ToDictionary(x => x.id, x => x);

                foreach (var hotel in hotelsDescription.hotels)
                {
                    HotelInfo convertHotel;
                    try
                    {
                        convertHotel = ConvertHotel(hotel, channelRequest.FirstOrDefault(x=>x.HotelId==hotel.id), roomsDescriptionDictionary, ratesDescriptionDictionary);
                    }
                    catch (Exception ex)
                    {
                        convertHotel = new Failure
                        {
                            HotelId = hotel.id,
                            Error = $"Failed to retreive hotel information (hotel id : {hotel.id}). Error message : {ex.Message}"
                        };
                    }
                    yield return convertHotel;
                }
            }
        }

        private static DescriptionResponseMessage Validate(DescriptionResponseMessage response)
        {
            if (response == null)
            {
                throw new Exception("Null response frome the service");
            }
            if (response.failure != null)
            {
                throw new Exception(response.failure.message);
            }
            return response;
        }

        #region ConvertEntities
        private HotelInfo ConvertHotel(HotelDescriptionResponseHotel hotel, ChannelRequest channelRequest, IReadOnlyDictionary<uint, RoomDescriptionResponseRoom> roomsDictionary, Dictionary<uint, RateDescriptionResponseRate> ratesDictionary)
        {
            return new HotelInfo
            {
                HotelPartnerId = channelRequest.HotelPartnerId,
                HotelId = hotel.id,
                PropertyName = hotel.name.Value,
                PropertyType = hotel.information.accomodation?.Value,
                PropertyWebsite = hotel.contact.webSites?.FirstOrDefault()?.Value,
                Phone = hotel.contact.phones?.FirstOrDefault(),
                RoomsCount = (int)hotel.information.roomCount,
                StarRating = (int)(hotel.information.rating?.stars ?? 0),
                Currencies = hotel.booking.currencies?.Select(x => x.code).ToArray(),
                CreditCards = hotel.booking.payment?.Select(x => x.type.ToString()).ToArray(),
                TimeZone = hotel.location.timeZone,
                Address = new Address
                {
                    Country = hotel.location.country?.code,
                    Street = hotel.location.addressLines?.addressLine.FirstOrDefault(),
                    City = hotel.location.city?.name,
                    Region = hotel.location.district?.name,
                    Zip = hotel.location.city?.postalCode,
                    Gps = (hotel.location.gps != null) ? hotel.location.gps.latitude + ", " + hotel.location.gps.longitude : null
                },
                Contact = new Contact
                {
                    Name = hotel.contact.managerName,
                    Phone = hotel.contact.phones?.FirstOrDefault(),
                    Fax = hotel.contact.faxes?.FirstOrDefault(),
                    Email = hotel.contact.emails?.FirstOrDefault()
                },
                Registration = hotel.reception == null ? null : new Registration
                {
                    CheckInTime = hotel.reception.checkInTime.ToString("HH:mm"),
                    CheckOutTime = hotel.reception.checkOutTime.ToString("HH:mm")
                },
                AgeSettings = new AgeSettings
                {
                    Adult = hotel.booking.guests.adults.minimumAge.ToString(),
                    Children =
                        hotel.booking.guests.children.minimumAge + "-" +
                        hotel.booking.guests.children.maximumAge,
                    Infant =
                        hotel.booking.guests.infants.minimumAge + "-" +
                        hotel.booking.guests.infants.maximumAge
                },
                Amenities = ConvertHotelAmenities(hotel),
                Vat = channelRequest.Vat,
                Rooms = ConvertRooms(roomsDictionary, channelRequest),
                Rates = ConvertRates(ratesDictionary, channelRequest)
            };
        }

        private Rate[] ConvertRates(Dictionary<uint, RateDescriptionResponseRate> ratesDictionary, ChannelRequest channelRequest)
        {
            return channelRequest.RateIds?.Select(rateId => new Rate
            {
                Name = ratesDictionary[rateId].name.Value,
                Code = (int) rateId,
                Currency = ratesDictionary[rateId].information.currency.code,
                Qualifier = ratesDictionary[rateId].information.qualifier?.Value
            }).ToArray();
        }

        private static AmenityGroup[] ConvertHotelAmenities(HotelDescriptionResponseHotel hotel)
        {
            return hotel.services?.Select(x => new AmenityGroup
            {
                Name = x.category.ToString(),
                Amenities = x.amenity.Select(s => s.name.ToString()).ToArray()
            }).ToArray();
        }

        private static Room[] ConvertRooms(IReadOnlyDictionary<uint, RoomDescriptionResponseRoom> roomsDictionary, ChannelRequest channelRequest)
        {
            var roomIds = new uint[] { 55210, 54601, 55213, 54600, 55211, 55214, 55209, 55212 };
            return channelRequest.RoomIds?.Select(roomId => new Room
            {
                Name = roomsDictionary[roomId].name.Value,
                Count = (int)roomsDictionary[roomId].information.totalCount,
                Type = roomsDictionary[roomId].information?.type?.Value,
                Category = roomsDictionary[roomId].information?.category?.Value,
                Qualifier = roomsDictionary[roomId].information?.qualifier?.Value,
                Beds = roomsDictionary[roomId].information?.beds?.bed.Select(bed => new Bed
                {
                    Type = bed.type.type.ToString(),
                    Count = (int)bed.count
                }).ToArray(),
                Occupancies =
                    roomsDictionary[roomId].occupancies?.Select(occupancy => ConvertOccupancy(occupancy.Item))
                        .ToArray(),
                Amenities = ConvertRoomAmenities(roomsDictionary[roomId])
            }).ToArray();
        }

        private static AmenityGroup[] ConvertRoomAmenities(RoomDescriptionResponseRoom roomDescriptionResponseRoom)
        {
            return roomDescriptionResponseRoom.services?.Select(x => new AmenityGroup
            {
                Name = x.category.ToString(),
                Amenities = x.amenity.Select(s => s.name.ToString()).ToArray()
            }).ToArray();
        }

        private static Occupancy ConvertOccupancy(object occupancy)
        {
            var detailed = occupancy as OccupancyDefinitionListOccupancyDetailed;
            if (detailed != null)
            {
                var detailedOccupancy = detailed;
                return new Occupancy
                {
                    MaxAdults = (int)detailedOccupancy.adultCount,
                    MaxChildren = (int)detailedOccupancy.childCount,
                    MaxInfants = (int)detailedOccupancy.infantCount,
                };
            }

            var simpleOccupancy = (OccupancyDefinitionListOccupancySimple)occupancy;
            return new Occupancy
            {
                MaxAdults = (int)simpleOccupancy.personCount
            };
        }
        #endregion

    }

    public class Result
    {
        public int HotelsCount { get; set; }
        public IEnumerable<HotelInfo> Hotels { get; set; }
    }
}