using System.Collections.Generic;
using System.Linq;

namespace DescriptionServiceClient
{
    public static class Extentions
    {
        public static IEnumerable<IEnumerable<T>> ByLot<T>(this IEnumerable<T> input, int blockSize)
        {
            var enumerator = input.GetEnumerator();

            while (enumerator.MoveNext())
            {
                yield return NextPartition(enumerator, blockSize);
            }
        }

        private static IEnumerable<T> NextPartition<T>(IEnumerator<T> enumerator, int blockSize)
        {
            do
            {
                yield return enumerator.Current;
            }
            while (--blockSize > 0 && enumerator.MoveNext());
        }
    }
}