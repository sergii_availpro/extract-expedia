﻿using System;
using System.CodeDom;
using System.Linq;
using DescriptionServiceClient.Extractions;

namespace DescriptionServiceClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                new ExtractHotelCsv().Run(args.Select(x => Convert.ToUInt32(x)).ToArray());
                //new ExtractAllHotelsForAGroup().Run();    
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Press any key to close");
            Console.ReadLine();
        }
    }
}
