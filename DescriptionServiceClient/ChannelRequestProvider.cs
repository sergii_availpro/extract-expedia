using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace DescriptionServiceClient
{
    public class ChannelRequestProvider
    {
        public IEnumerable<ChannelRequest> GetChannelRequest(uint groupId, IEnumerable<uint> tenHotels)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AvpConfigurationConnectionString"].ConnectionString;
            var sql =
$@"SELECT groupid,hotelid,[Content]
FROM[Availpro.Configuration].[Connectivity].[ChannelRequestTickets]
where groupid = {groupId} and hotelid in ({string.Join(",", tenHotels)})";

            var ds = new DataSet();

            using (var connection = new OleDbConnection(connectionString))
            using (var command = new OleDbCommand(sql, connection))
            using (var adapter = new OleDbDataAdapter(command))
            {
                adapter.Fill(ds);
            }

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var channelRequestString = (string)row["Content"];
                var doc = XDocument.Parse(channelRequestString);

                yield return new ChannelRequest
                {
                    GroupId = Convert.ToUInt32(row["groupid"]),
                    HotelId = Convert.ToUInt32(row["hotelid"]),
                    HotelPartnerId = doc.Root.Attribute("HotelPartnerId").Value,
                    Vat =
                        doc.Descendants("Information")
                            .Select(x => new { Name = x.Attribute("Name").Value, x.Attribute("Value").Value })
                            .FirstOrDefault(x => x.Name == "VatRate")
                            ?.Value,
                    RoomIds = doc.Descendants("RoomId").Select(x => uint.Parse(x.Value)).ToArray(),
                    RateIds = doc.Descendants("RateId").Select(x => uint.Parse(x.Value)).ToArray()
                };
            }
        }
    }

    public class ChannelRequest
    {
        public uint GroupId { get; set; }
        public uint HotelId { get; set; }
        public string Vat { get; set; }
        public uint[] RoomIds { get; set; }
        public uint[] RateIds { get; set; }
        public string HotelPartnerId { get; set; }
    }
}